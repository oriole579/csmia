package com.example.google.ikea;

import android.app.Application;
import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by dharm on 12/01/2017.
 */

public class applicationClass extends Application {

    private static applicationClass mInstance;
    public static Context mContext;
    private static String UserId;
    NFC nfc;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mContext = getApplicationContext();
    }

    public static Context getContext() {
        return mContext;
    }

    public static synchronized applicationClass getInstance() {
        return mInstance;
    }

    public void setUserId(String UserID){
        this.UserId = UserID;

    }

    public String setUserId(){
        return UserId;
    }


    public String yymmdd()
    {
        Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = simpleDateFormat.format(calendar.getTime());
        return date;
    }

    public String yymmddhhmm()
    {
        Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String date = simpleDateFormat.format(calendar.getTime());
        return date;
    }

    public String yymmddhhmmss()
    {
        Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = simpleDateFormat.format(calendar.getTime());
        return date;
    }
    public String imageVariable(){
        return "yes";
    }
    public String completedView(){
        return "yes";
    }
    public boolean checkLog(){ return false; }
    public boolean writeJsonFile(){ return false; }
    public String AutoSync(){ return "false"; }
    public int AutoSyncFreq(){ return 15; }
    public boolean fabButton(){ return false; }
    public String Notification(){ return "true"; }
    public String defaultNFC(){return "QR";}
    public String insertTask(){return "insertTask.php";}
    public String urlString(){
        return "https://www.punctualiti.net/citrix/";
    }
}
