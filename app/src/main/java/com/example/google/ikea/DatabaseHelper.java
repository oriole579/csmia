package com.example.google.ikea;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Intel on 23-03-2017.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    static final boolean LOG = new applicationClass().checkLog();
    applicationClass applicationClass = new applicationClass();
    public DatabaseHelper(Context context) {
        super(context, "facilitymate.db", null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE Activity_Frequency (Id INTEGER PRIMARY KEY AUTOINCREMENT,Site_Location_Id TEXT,Frequency_Auto_Id TEXT,YearStartson TEXT,TimeStartson TEXT,TimeEndson TEXT,Activity_Duration INTEGER,Grace_Duration_Before INTEGER,Grace_Duration_After INTEGER,RepeatEveryDay INTEGER,RepeatEveryMin INTEGER,RepeatEveryMonth TEXT,Verified INTEGER,Assign_Days TEXT,Asset_Activity_Linking_Id TEXT,RecordStatus TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Asset_Activity_Linking (Id INTEGER PRIMARY KEY AUTOINCREMENT,Site_Location_Id TEXT,Auto_Id TEXT,Asset_Id TEXT,Activity_Id TEXT,RecordStatus TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Activity_Master (Id INTEGER PRIMARY KEY AUTOINCREMENT,Site_Location_Id TEXT,Auto_Id TEXT,Form_Id TEXT,Activity_Name TEXT,RecordStatus TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Asset_Activity_AssignedTo (Id INTEGER PRIMARY KEY AUTOINCREMENT,Site_Location_Id TEXT,Auto_Id TEXT,Assigned_To_User_Id TEXT,Assigned_To_User_Group_Id TEXT,Asset_Activity_Linking_Id TEXT,RecordStatus TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Login_Details (Id INTEGER PRIMARY KEY,User_Id TEXT,Employee_Id TEXT, Company_Customer_Id TEXT, Username TEXT,Password TEXT,Employee_Name TEXT,Verify INTEGER)");
        sqLiteDatabase.execSQL("CREATE TABLE User_Group (Id INTEGER PRIMARY KEY,User_Group_Id TEXT,Group_Name TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Site_Details (Id INTEGER PRIMARY KEY ,Auto_Id TEXT,Site_Name_Label TEXT,Site_Name_Value TEXT,Assigned_To_User_Id TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Form_Structure (Id INTEGER PRIMARY KEY,Field_Id TEXT,Site_Location_Id TEXT,Form_Id TEXT,Field_Label TEXT,Field_Type TEXT,Field_Options TEXT,FixedValue Text,Mandatory INTEGER,sid INTEGER,sections TEXT,Display_Order INTEGER,FormType TEXT,SafeRange INTEGER,Record_Status TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Asset_Details (Id INTEGER PRIMARY KEY ,Asset_Id TEXT,Site_Location_Id TEXT,Asset_Code TEXT,Asset_Name TEXT,Asset_Location TEXT,Asset_Status_Id TEXT,Status TEXT,Manual_Time TEXT, Asset_Update_Time TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Asset_Status (Id INTEGER PRIMARY KEY,Asset_Status_Id TEXT,Status TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Parameter (Id INTEGER PRIMARY KEY,Site_Location_Id TEXT,Activity_Frequency_Id TEXT, Form_Id TEXT,Form_Structure_Id TEXT, Field_Limit_From TEXT,Field_Limit_To TEXT,Threshold_From TEXT,Threshold_To TEXT,Validation_Type TEXT,Critical INTEGER,Field_Option_Id TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Meter_Reading(Id INTEGER PRIMARY KEY,Site_Location_Id TEXT,Task_Id TEXT,Asset_Id TEXT,Form_Structure_Id TEXT,Reading TEXT,UOM TEXT,Reset INTEGER,Activity_Frequency_Id TEXT,Task_Start_At TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Data_Posting(Id INTEGER PRIMARY KEY,Site_Location_Id TEXT,Task_Id TEXT,Form_Id TEXT,Form_Structure_Id TEXT,Parameter_Id TEXT,Value TEXT,Remark TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Task_Details (Id INTEGER PRIMARY KEY,Auto_Id TEXT,Company_Customer_Id TEXT,Site_Location_Id TEXT,Activity_Frequency_Id TEXT,Asset_Activity_Linking_Auto_Id TEXT,Activity_Master_Auto_Id TEXT,Asset_Activity_AssignedTo_Auto_Id TEXT,Task_Start_At TEXT,Task_Scheduled_Date TEXT,Task_Status TEXT,Scan_Type TEXT,Assigned_To TEXT,Assigned_To_User_Id TEXT,Assigned_To_User_Group_Id TEXT,Asset_Id TEXT,From_Id TEXT,EndDateTime TEXT,Asset_Code TEXT,Asset_Name TEXT,Asset_Location TEXT,Asset_Status TEXT, Activity_Name TEXT,Remarks Text,Verified INTEGER,RecordStatus TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Ticket_Master (ID INTEGER PRIMARY KEY ,Site_Location_Id TEXT,Company_Customer_Id TEXT,Created_Source TEXT,Created_At TEXT,ticket_Subject TEXT,ticket_Content TEXT,ticket_Priority TEXT,ticket_Type TEXT,Task_Type TEXT,Ticket_Raise_By TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE AlertMaster (ID INTEGER PRIMARY KEY ,Site_Location_Id TEXT,Task_Id TEXT,Form_Id TEXT,Form_Structure_Id TEXT,Alert_Type TEXT,Asset_Name TEXT,Activity_Name TEXT,Activity_Frequency_Id TEXT,Task_Status TEXT,Task_Start_At TEXT,Task_Scheduled_Date TEXT,Created_By_Id TEXT,Assigned_To_User_Group_Id TEXT,Critical TEXT,TaskType TEXT,ViewFlag TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Task_Details_Server (Id INTEGER PRIMARY KEY,Task_Id TEXT,Site_Location_Id TEXT,Activity_Frequency_Id TEXT,Task_Scheduled_Date TEXT,Task_Status TEXT,Assigned_To_User_Id TEXT,Task_Start_At TEXT,Assigned_To_User_Group_Id TEXT,Remarks TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Task_Selfie (ID INTEGER PRIMARY KEY ,Site_Location_Id TEXT, Task_Id TEXT,Task_Server_Id TEXT, Image_Selfie BLOB,Image_Type TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE AssetStatusLog (Id INTEGER PRIMARY KEY ,Asset_Id TEXT,Site_Location_Id TEXT,Previous_Asset_Status_Id TEXT,Asset_Status_Id TEXT,Manual_Time TEXT,Asset_Updated_Time TEXT,Remark TEXT,Assigned_To_User_Id TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE Settings (Id INTEGER PRIMARY KEY,User_Id TEXT,Site_Location_Id TEXT,User_Group_Id TEXT,Notification TEXT,Scan_Type TEXT,Site_Name TEXT,Site_URL TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE AutoSync (Id INTEGER PRIMARY KEY,Site_Location_Id TEXT,SyncStatus TEXT,SyncFreq TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE RefrenseTable (Id INTEGER PRIMARY KEY,Site_Location_Id TEXT,Auto_Id TEXT,Record_Id TEXT,Table_Name TEXT,Updated_Id TEXT,DateTime TEXT,UpdatedStatus TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE UserSiteLinking (Id INTEGER PRIMARY KEY,Linking_Auto_Id TEXT,User_Id TEXT,Site_Location_Id TEXT,User_Group_Id TEXT,User_Role_Id TEXT,User_Right_Id TEXT,Created_DateTime TEXT,Deleted_DateTime TEXT,Record_Status TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE site_imagelist (Id INTEGER PRIMARY KEY,Auto_Id TEXT,Site_Location_Id TEXT,Image_Name TEXT,Record_Status TEXT)");
        //sqLiteDatabase.execSQL("CREATE TABLE Task_Frequency (Id INTEGER PRIMARY KEY AUTOINCREMENT,Site_Location_Id TEXT,Frequency_Auto_Id TEXT,Asset_ID TEXT,Form_Id TEXT,Assigned_To_User_Id TEXT,Assigned_To_User_Group_Id TEXT,Activity_Name TEXT,YearStartson TEXT,TimeStartson TEXT,TimeEndson TEXT,Activity_Duration INTEGER,Grace_Duration_Before INTEGER,Grace_Duration_After INTEGER,RepeatEveryDay INTEGER,RepeatEveryMin INTEGER,RepeatEveryMonth TEXT,Verified INTEGER,Assign_Days TEXT,UpdatedStatus TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        if (newVersion > oldVersion) {
            sqLiteDatabase.execSQL("ALTER TABLE Task_Frequency ADD COLUMN RepeatEveryMonth TEXT");
        }
    }

    public int getassetCount(String assetCode){
        int count = 0;
        String selectQuery = "SELECT Asset_Id FROM Asset_Details where Asset_Code = '"+assetCode+"'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        count = cursor.getCount();
        cursor.close();
        database.close();
        return count;
    }


    public int getRecord(String Site_Location_Id){
        int record_id =0;
        try {
            SQLiteDatabase database = this.getWritableDatabase();
            String query="SELECT Record_Id FROM RefrenseTable WHERE Record_Id = (SELECT MAX(Record_Id) FROM RefrenseTable) AND Site_Location_Id='"+ Site_Location_Id+"' ";
            Cursor res =database.rawQuery(query, null);

            if(res.getCount()!=0){
                if (res.moveToFirst()) {
                    do {
                        record_id = res.getInt(0);
                    } while (res.moveToNext());
                }

            }
            res.close();
            database.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return record_id;
    }


    public boolean insertRadioBitmap(Bitmap selfie, String taskID,String SiteId,String Form_Structure_Id)  {
        boolean image = false;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        selfie.compress(Bitmap.CompressFormat.JPEG, 100, out);

        ByteArrayOutputStream meterOut = new ByteArrayOutputStream();
        byte[] selfieBuffer=out.toByteArray();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values;
        try
        {
            values = new ContentValues();
            values.put("Image_Selfie", selfieBuffer);
            values.put("Task_Id", taskID);
            values.put("UpdatedStatus","no");
            values.put("Site_Location_Id",SiteId);
            //values.put("Form_Structure_Id",Form_Structure_Id);
            long i = db.insert("Task_Selfie", null, values);
            if(i == -1)
                image =  false;
            else
                image =  true;
            Log.i("Insert", i + "");
            db.close();
            // Insert into database successfully.
        }
        catch (SQLiteException e)
        {
            e.printStackTrace();

        }
        return image;


    }

    public int refrenseTableCount(String site_Id){
        int count =0;
        SQLiteDatabase database = this.getWritableDatabase();
        String query="select * from RefrenseTable where Site_Location_Id='"+site_Id+"' AND UpdatedStatus='no'";
        Cursor res =database.rawQuery(query, null);
        count=res.getCount();

        return count;
    }


    public boolean insertVal (String uid,String SiteId, String User_Group_Id, String Notification,String Scan_Type,String Site_Name,String Site_URL){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("User_Id",uid);
        contentValues.put("Site_Location_Id",SiteId);
        contentValues.put("User_Group_Id",User_Group_Id);
        contentValues.put("Notification", Notification);
        contentValues.put("Scan_Type", Scan_Type);
        contentValues.put("Site_Name", Site_Name);
        contentValues.put("Site_URL", Site_URL);
        long result = db.insert("Settings", null, contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public boolean insertAutoSync (String SiteId,String SyncStatus,int SyncFreq){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Site_Location_Id",SiteId);
        contentValues.put("SyncStatus",SyncStatus);
        contentValues.put("SyncFreq",SyncFreq);
        long result = db.insert("AutoSync", null, contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }

    public String ScanType(String User_Id){
        String Scan_Type="";
        try {
            String query = "SELECT Scan_Type FROM Settings Where User_Id ='"+User_Id+"'";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    Scan_Type=res.getString(0);
                } while (res.moveToNext());
            }
            res.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Scan_Type;
    }

    public String UserGroupId(String User_Id){
        String UserGroupId="";
        try {
            String query = "SELECT User_Group_Id FROM Settings Where User_Id ='"+User_Id+"'";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    UserGroupId=res.getString(0);
                } while (res.moveToNext());
            }
            res.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return UserGroupId;
    }

    public String Notification(String User_Id){
        String notification="";
        try {
            String query = "SELECT Notification FROM Settings Where User_Id ='"+User_Id+"'";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    notification=res.getString(0);
                } while (res.moveToNext());
            }
            res.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notification;
    }

    public String SiteName(String User_Id){
        String Site_Name="";
        try {
            String query = "SELECT Site_Name FROM Settings Where User_Id ='"+User_Id+"'";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    Site_Name=res.getString(0);
                } while (res.moveToNext());
            }
            res.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Site_Name;
    }
    public String SiteURL(String User_Id){
        String Site_URL="";
        try {
            String query = "SELECT Site_URL FROM Settings Where User_Id ='"+User_Id+"'";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    Site_URL=res.getString(0);

                } while (res.moveToNext());
            }
            res.close();
            //db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Site_URL;
    }

    public String Site_IdUserSiteLinking(String User_Id){
        String Site_Location_Id="";
        try {
            String query = "SELECT Site_Location_Id FROM UserSiteLinking Where User_Id ='"+User_Id+"'";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    Site_Location_Id=res.getString(0);
                } while (res.moveToNext());
            }
            res.close();
            // db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Site_Location_Id;
    }

    public String Site_Location_Id(String User_Id){
        String Site_Location_Id="";
        try {
            String query = "SELECT Site_Location_Id FROM Settings Where User_Id ='"+User_Id+"'";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    Site_Location_Id=res.getString(0);
                } while (res.moveToNext());
            }
            res.close();
           // db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Site_Location_Id;
    }

    public void UpdateSiteName(String Site_Name,String UserGroupId,String User_Id,String Site_URL,String Site_Location_Id){
        ContentValues contentValues = new ContentValues();
        SQLiteDatabase db = getWritableDatabase();
        contentValues.put("Site_Name", Site_Name);
        contentValues.put("Site_URL", Site_URL);
        contentValues.put("User_Group_Id", UserGroupId);
        contentValues.put("Site_Location_Id", Site_Location_Id);
        db.update("Settings", contentValues, "User_Id ='" + User_Id + "'", null);
        db.close();
    }

    public String AutoSyncSetting(){
        String Autosync="";
        try {
            String query = "SELECT * FROM AutoSync";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    Autosync=res.getString(res.getColumnIndex("SyncStatus"));

                } while (res.moveToNext());
            }
            res.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Autosync;
    }

    public String AutoSyncFreq(){
        String Autosync="";
        try {
            String query = "SELECT * FROM AutoSync";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    Autosync=res.getString(res.getColumnIndex("SyncFreq"));

                } while (res.moveToNext());
            }
            res.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Autosync;
    }

    public int Verify(String User_Id){
        int Verify=0;
        try {
            String query = "SELECT * FROM Login_Details where User_Id='"+User_Id+"'";
            SQLiteDatabase db = getWritableDatabase();
            Cursor res =db.rawQuery(query, null);
            if (res.moveToFirst()) {
                do {
                    Verify=res.getInt(res.getColumnIndex("Verify"));
                } while (res.moveToNext());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Verify;
    }
    public boolean insertTicket(String siteLocationId,String Company_Customer_Id,String createdSource,String CreatedAt,String ticketSubject,String ticketContent,String ticketPriority,String ticket_type,String updated_Status,String Ticket_Raise_By) {

        SQLiteDatabase db =this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("Site_Location_Id", siteLocationId);
        contentValues.put("Company_Customer_Id", Company_Customer_Id);
        contentValues.put("Created_Source", createdSource);
        contentValues.put("Created_At", CreatedAt);
        contentValues.put("ticket_Subject", ticketSubject);
        contentValues.put("ticket_Content", ticketContent);
        contentValues.put("ticket_Priority", ticketPriority);
        contentValues.put("ticket_Type", ticket_type);
        contentValues.put("Task_Type", "");
        contentValues.put("Ticket_Raise_By", Ticket_Raise_By);
        contentValues.put("UpdatedStatus", updated_Status);

        long resultset = db.insert("Ticket_Master", null, contentValues);
        if(resultset == -1){
            return false;}
        else{
            return true;}
    }

    public int getassetTaskCount(String assetCode,String UserGroupId){
        int count = 0;
        String selectQuery = "Select * FROM Task_Details WHERE Asset_Code ='"+assetCode+"' and Assigned_To_User_Group_Id IN ("+UserGroupId+") AND RecordStatus = 'I'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        count = cursor.getCount();
        cursor.close();
        database.close();
        return count;
    }


    public String getassetStatus(String assetCode){
        String assetStatus = "";
        String selectQuery = "SELECT Task_Status FROM Task_Details WHERE Asset_Code = '"+assetCode+"' AND Task_Scheduled_Date <='"+ applicationClass.yymmddhhmm()+"' AND EndDateTime >= '"+ applicationClass.yymmddhhmm()+"' AND Task_Status !='Unplanned' AND RecordStatus = 'I'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if(cursor.getCount() !=0){
            if(cursor.moveToNext()){
                assetStatus = cursor.getString(0);
            }
        }else {
            String Query = "SELECT Task_Status FROM Task_Details WHERE Asset_Code = '"+assetCode+"' AND Task_Status !='Unplanned'";
            Cursor cursorforCount = database.rawQuery(Query, null);
            if(cursorforCount.getCount() !=0) {
                if (cursorforCount.moveToNext()) {
                    assetStatus = cursorforCount.getString(0);
                }
            } else{
                assetStatus = "Not found";
            }
        }

        return assetStatus;
    }


    /*public String getassetStatus(String assetCode){
        String assetStatus = "";
        String selectQuery = "SELECT Task_Status FROM Task_Details WHERE Asset_Code = '"+assetCode+"'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);

        if(cursor.moveToNext()){
            assetStatus = cursor.getString(0);
            Log.d("taskStatus","1 "+cursor.getCount()+" 2 "+cursor.getString(0));

        }
        return assetStatus;
    }*/
    public String getfieldId(int id) {
        String formType = " ";
        String selectQuery = "SELECT Field_Id FROM Form_Structure WHERE Id = '"+id+"'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if(cursor.moveToNext()){
            formType = cursor.getString(0);
        }
        cursor.close();
        database.close();
        return formType;
    }

    public String getFormId(String Field_Id) {
        String Form_Id = " ";
        String selectQuery = "SELECT Form_Id FROM Form_Structure WHERE Field_Id = '"+Field_Id+"'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if(cursor.moveToNext()){
            Form_Id = cursor.getString(0);
        }
        cursor.close();
        database.close();
        return Form_Id;
    }

    public String getfieldLabel(int id) {
        String formType = " ";
        String selectQuery = "SELECT Field_Label FROM Form_Structure WHERE Id = '"+id+"'";
        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if(cursor.moveToNext()){
            formType = cursor.getString(0);
        }
        cursor.close();
        database.close();
        return formType;
    }

    public String lastMultiMeterReading(String autoId,String fieldId){
        StringBuffer buffer = new StringBuffer();
        String reading ="";
        String uomValue ="";
        SQLiteDatabase database = this.getWritableDatabase();
        String updateQuery = "Select Reading, UOM,Task_Start_At  FROM Meter_Reading WHERE  Asset_Id = '"+autoId+"' AND Form_Structure_Id='"+fieldId+"'  ORDER BY Id DESC LIMIT 1";
        Cursor cursor = database.rawQuery(updateQuery, null);
        if (cursor.moveToFirst()) {
            do {
                reading =  cursor.getString(0);
                uomValue =  cursor.getString(1);
                buffer.append(reading);
                buffer.append(" ");
                buffer.append(uomValue);
            } while (cursor.moveToNext());

        }
        cursor.close();
        database.close();
        if(reading.equals("")){
            return "No Previous Reading";
        }else {
            return buffer.toString();
        }

    }

    public boolean updatedTaskDetails(String Auto_Id,String Task_Status, String Conditional_Status,String Task_Start_At,String Scan_Type,String userId,String Remarks){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Task_Status", Task_Status);
        contentValues.put("Task_Start_At", Task_Start_At);
        contentValues.put("Scan_Type", Scan_Type);
        contentValues.put("Assigned_To_User_Id",userId);
        contentValues.put("UpdatedStatus", "no");
        contentValues.put("Remarks", Remarks);
        long resultset = database.update("Task_Details", contentValues, "Auto_Id ='" + Auto_Id + "' AND  Task_Status='"+Conditional_Status+"'", null);
        database.close();
        if(resultset == -1)
            return false;
        else
            return true;

    }


/*
    public boolean updatedTaskDetails(String Auto_Id,String Task_Status,String Task_Start_At,String Scan_Type,String userId,String Remarks){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Task_Status", Task_Status);
        contentValues.put("Task_Start_At", Task_Start_At);
        contentValues.put("Scan_Type", Scan_Type);
        contentValues.put("Assigned_To_User_Id",userId);
        contentValues.put("UpdatedStatus", "no");
        contentValues.put("Remarks", Remarks);
        long resultset = database.update("Task_Details", contentValues, "Auto_Id ='" + Auto_Id + "' AND  Task_Status='Pending'", null);
        database.close();
        if(resultset == -1)
            return false;
        else
            return true;

    }
*/

    public boolean updatedTaskCancelled(String Asset_Id,String Task_Status,String Task_Start_At,String Scan_Type,String Asset_Status){
        SimpleDateFormat YMDHMDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Calendar calenderCurrent = Calendar.getInstance();

        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Task_Status",Task_Status);
        contentValues.put("Task_Start_At",Task_Start_At);
        contentValues.put("Scan_Type",Scan_Type);
        contentValues.put("Asset_Status",Asset_Status);
        long resultset = -1;
        if(Task_Status.equals("Cancelled")) {
            resultset = database.update("Task_Details",contentValues, "Asset_Id ='" + Asset_Id + "' AND  Task_Status='Pending' ", null);
            database.close();

        }else if(Task_Status.equals("Pending")) {
            resultset = database.update("Task_Details",contentValues, "Asset_Id ='" + Asset_Id + "' AND  Task_Status='Cancelled'  AND datetime(EndDateTime) >= datetime('"+YMDHMDateFormat.format(calenderCurrent.getTime())+"')", null);
            database.close();
        }
        if (resultset == -1)
            return false;
        else
            return true;

    }

    public boolean updatedTaskCancelledCart(String TaskId){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("UpdatedStatus", "no");
        long resultset = database.update("Task_Details", contentValues, "Auto_Id ='" + TaskId + "' AND  Task_Status='Cancelled' AND UpdatedStatus IS null", null);
        database.close();
        if(resultset == -1)
            return false;
        else
            return true;
    }


    public JSONArray composeJSONfortaskDetailsNew(String UserId,String Task_Status){
        JSONArray UploadArray = new JSONArray();
        JSONObject TaskDetails = new JSONObject();
        JSONObject MeterReading = new JSONObject();
        JSONObject DataValue = new JSONObject();
        JSONObject AlertData = new JSONObject();

        JSONArray TaskJsonArray = new JSONArray();
        JSONArray MeterReadingArray = new JSONArray();
        JSONArray DataPostingArray = new JSONArray();
        JSONArray AlertArray = new JSONArray();
        try {


            String[]loggerData = new LoggerFile().loggerFunction(UserId);

            String selectQuery = "SELECT  * FROM Task_Details where UpdatedStatus = 'no' and Assigned_To_User_Id='"+UserId+"' and Task_Status='"+Task_Status+"' ORDER BY Task_Scheduled_Date LIMIT 5" ;
            SQLiteDatabase database = this.getWritableDatabase();

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    JSONObject TaskJsonObject = new JSONObject();
                    String TaskID= cursor.getString(cursor.getColumnIndex("Auto_Id"));
                    TaskJsonObject.put("Auto_Id", cursor.getString(cursor.getColumnIndex("Auto_Id")));
                    TaskJsonObject.put("Company_Customer_Id",cursor.getString(cursor.getColumnIndex("Company_Customer_Id")));
                    TaskJsonObject.put("Site_Location_Id",cursor.getString(cursor.getColumnIndex("Site_Location_Id")));
                    TaskJsonObject.put("Activity_Frequency_Id", cursor.getString(cursor.getColumnIndex("Activity_Frequency_Id")));
                    TaskJsonObject.put("Task_Start_At", cursor.getString(cursor.getColumnIndex("Task_Start_At")));
                    TaskJsonObject.put("Task_Scheduled_Date", cursor.getString(cursor.getColumnIndex("Task_Scheduled_Date")));
                    TaskJsonObject.put("Task_Status", cursor.getString(cursor.getColumnIndex("Task_Status")));
                    TaskJsonObject.put("Asset_Id", cursor.getString(cursor.getColumnIndex("Asset_Id")));
                    TaskJsonObject.put("Scan_Type", cursor.getString(cursor.getColumnIndex("Scan_Type")));
                    TaskJsonObject.put("Assigned_To", cursor.getString(cursor.getColumnIndex("Assigned_To")));
                    TaskJsonObject.put("Assigned_To_User_Id", cursor.getString(cursor.getColumnIndex("Assigned_To_User_Id")));
                    TaskJsonObject.put("Assigned_To_User_Group_Id", cursor.getString(cursor.getColumnIndex("Assigned_To_User_Group_Id")));
                    TaskJsonObject.put("Record_Status",loggerData[0] );
                    TaskJsonObject.put("Last_Date_Time",loggerData[1]);
                    TaskJsonObject.put("Last_IP",loggerData[2]);
                    TaskJsonObject.put("Last_User_Id",loggerData[3]);
                    TaskJsonObject.put("Update_Location",loggerData[4]);
                    TaskJsonObject.put("Apk_Web_Version", loggerData[5]);
                    TaskJsonObject.put("GeoLoc", loggerData[6]);


                    String selectQuery1 = "SELECT  Meter_Reading.* FROM Meter_Reading LEFT JOIN  Task_Details ON Meter_Reading.Task_Id = Task_Details.Auto_Id WHERE  Meter_Reading.Task_Id ='"+TaskID+"' AND Meter_Reading.UpdatedStatus = 'no'  " ;

                    Cursor cursorMeterReading = database.rawQuery(selectQuery1, null);

                    if (cursorMeterReading.moveToFirst()) {
                        do {
                            JSONObject MeterReadingJsonObject = new JSONObject();

                            MeterReadingJsonObject.put("Task_Id",cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Task_Id")));
                            MeterReadingJsonObject.put("Asset_Id", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Asset_Id")));
                            MeterReadingJsonObject.put("Form_Structure_Id", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Form_Structure_Id")));
                            MeterReadingJsonObject.put("Reading", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Reading")));
                            MeterReadingJsonObject.put("UOM", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("UOM")));
                            MeterReadingJsonObject.put("Reset", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Reset")));
                            MeterReadingJsonObject.put("Activity_Frequency_Id", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Activity_Frequency_Id")));
                            MeterReadingJsonObject.put("Record_Status",loggerData[0] );
                            MeterReadingJsonObject.put("Last_Date_Time",loggerData[1]);
                            MeterReadingJsonObject.put("Last_IP",loggerData[2]);
                            MeterReadingJsonObject.put("Last_User_Id",loggerData[3]);
                            MeterReadingJsonObject.put("Update_Location",loggerData[4]);
                            MeterReadingJsonObject.put("Apk_Web_Version", loggerData[5]);
                            MeterReadingJsonObject.put("GeoLoc", loggerData[6]);
                            MeterReadingArray.put(MeterReadingJsonObject);
                        } while (cursorMeterReading.moveToNext());

                    }
                    cursorMeterReading.close();

                    String selectQueryData = "SELECT  Data_Posting.* FROM Data_Posting LEFT JOIN  Task_Details ON Data_Posting.Task_Id = Task_Details.Auto_Id WHERE Data_Posting.UpdatedStatus = 'no' AND Data_Posting.Task_Id ='"+TaskID+"' ";
                    Cursor cursorDataPosting = database.rawQuery(selectQueryData, null);
                    if (cursorDataPosting.moveToFirst()) {
                        do {
                            JSONObject DatPostingJsonObject = new JSONObject();
                            DatPostingJsonObject.put("Task_Id", cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Task_Id")));
                            DatPostingJsonObject.put("Form_Id",cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Form_Id")));
                            DatPostingJsonObject.put("Form_Structure_Id",cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Form_Structure_Id")));
                            DatPostingJsonObject.put("Parameter_Id", cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Parameter_Id")));
                            DatPostingJsonObject.put("Value", cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Value")));
                            DatPostingJsonObject.put("Record_Status",loggerData[0] );
                            DatPostingJsonObject.put("Last_Date_Time",loggerData[1]);
                            DatPostingJsonObject.put("Last_IP",loggerData[2]);
                            DatPostingJsonObject.put("Last_User_Id",loggerData[3]);
                            DatPostingJsonObject.put("Update_Location",loggerData[4]);
                            DatPostingJsonObject.put("Apk_Web_Version", loggerData[5]);
                            DatPostingJsonObject.put("GeoLoc", loggerData[6]);
                            DataPostingArray.put(DatPostingJsonObject);
                        } while (cursorDataPosting.moveToNext());
                    }
                    cursorDataPosting.close();

                    String selectQueryAlert = "SELECT * FROM AlertMaster WHERE Task_Id='"+TaskID+"'";
                    Cursor cursorAlert = database.rawQuery(selectQueryAlert, null);
                    if (cursorAlert.moveToFirst()) {
                        do {
                            JSONObject AlertJsonObject = new JSONObject();
                            AlertJsonObject.put("Task_Id",cursorAlert.getString(cursorAlert.getColumnIndex("Task_Id")));
                            AlertJsonObject.put("Form_Id", cursorAlert.getString(cursorAlert.getColumnIndex("Form_Id")));
                            AlertJsonObject.put("Form_Structure_Id", cursorAlert.getString(cursorAlert.getColumnIndex("Form_Structure_Id")));
                            AlertJsonObject.put("Alert_Type", cursorAlert.getString(cursorAlert.getColumnIndex("Alert_Type")));
                            AlertJsonObject.put("Created_By_Id", cursorAlert.getString(cursorAlert.getColumnIndex("Created_By_Id")));
                            AlertJsonObject.put("TaskType", cursorAlert.getString(cursorAlert.getColumnIndex("TaskType")));
                            AlertJsonObject.put("Critical", cursorAlert.getString(cursorAlert.getColumnIndex("Critical")));
                            AlertJsonObject.put("Record_Status",loggerData[0] );
                            AlertJsonObject.put("Last_Date_Time",loggerData[1]);
                            AlertJsonObject.put("Last_IP",loggerData[2]);
                            AlertJsonObject.put("Last_User_Id",loggerData[3]);
                            AlertJsonObject.put("Update_Location",loggerData[4]);
                            AlertJsonObject.put("Apk_Web_Version", loggerData[5]);
                            AlertJsonObject.put("GeoLoc", loggerData[6]);
                            AlertArray.put(AlertJsonObject);
                        } while (cursorAlert.moveToNext());
                    }
                    cursorAlert.close();
                    TaskJsonArray.put(TaskJsonObject);
                } while (cursor.moveToNext());
            }cursor.close();
            database.close();

        }catch (Exception E){
            E.printStackTrace();
        }

        try {

            TaskDetails.put("TaskDetails",TaskJsonArray);
            MeterReading.put("MeterReading",MeterReadingArray);
            DataValue.put("DataValue",DataPostingArray);
            AlertData.put("AlertData",AlertArray);

            UploadArray.put(TaskDetails);
            UploadArray.put(MeterReading);
            UploadArray.put(DataValue);
            UploadArray.put(AlertData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return UploadArray;
    }


    public JSONArray composeJSONfortaskDetailsReconfigNew(String UserId,String Task_Status){
        JSONArray UploadArray = new JSONArray();
        JSONObject TaskDetails = new JSONObject();
        JSONObject MeterReading = new JSONObject();
        JSONObject DataValue = new JSONObject();
        JSONObject AlertData = new JSONObject();

        JSONArray TaskJsonArray = new JSONArray();
        JSONArray MeterReadingArray = new JSONArray();
        JSONArray DataPostingArray = new JSONArray();
        JSONArray AlertArray = new JSONArray();
        try {


            String[]loggerData = new LoggerFile().loggerFunction(UserId);

            String selectQuery = "SELECT  * FROM Task_Details where UpdatedStatus = 'no' and Task_Status='"+Task_Status+"' ORDER BY Task_Scheduled_Date LIMIT 5" ;
            SQLiteDatabase database = this.getWritableDatabase();

            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    JSONObject TaskJsonObject = new JSONObject();
                    String TaskID= cursor.getString(cursor.getColumnIndex("Auto_Id"));
                    TaskJsonObject.put("Auto_Id", cursor.getString(cursor.getColumnIndex("Auto_Id")));
                    TaskJsonObject.put("Company_Customer_Id",cursor.getString(cursor.getColumnIndex("Company_Customer_Id")));
                    TaskJsonObject.put("Site_Location_Id",cursor.getString(cursor.getColumnIndex("Site_Location_Id")));
                    TaskJsonObject.put("Activity_Frequency_Id", cursor.getString(cursor.getColumnIndex("Activity_Frequency_Id")));
                    TaskJsonObject.put("Task_Start_At", cursor.getString(cursor.getColumnIndex("Task_Start_At")));
                    TaskJsonObject.put("Task_Scheduled_Date", cursor.getString(cursor.getColumnIndex("Task_Scheduled_Date")));
                    TaskJsonObject.put("Task_Status", cursor.getString(cursor.getColumnIndex("Task_Status")));
                    TaskJsonObject.put("Asset_Id", cursor.getString(cursor.getColumnIndex("Asset_Id")));
                    TaskJsonObject.put("Scan_Type", cursor.getString(cursor.getColumnIndex("Scan_Type")));
                    TaskJsonObject.put("Assigned_To", cursor.getString(cursor.getColumnIndex("Assigned_To")));
                    TaskJsonObject.put("Assigned_To_User_Id", cursor.getString(cursor.getColumnIndex("Assigned_To_User_Id")));
                    TaskJsonObject.put("Assigned_To_User_Group_Id", cursor.getString(cursor.getColumnIndex("Assigned_To_User_Group_Id")));
                    TaskJsonObject.put("Record_Status",loggerData[0] );
                    TaskJsonObject.put("Last_Date_Time",loggerData[1]);
                    TaskJsonObject.put("Last_IP",loggerData[2]);
                    TaskJsonObject.put("Last_User_Id",loggerData[3]);
                    TaskJsonObject.put("Update_Location",loggerData[4]);
                    TaskJsonObject.put("Apk_Web_Version", loggerData[5]);
                    TaskJsonObject.put("GeoLoc", loggerData[6]);


                    String selectQuery1 = "SELECT  Meter_Reading.* FROM Meter_Reading LEFT JOIN  Task_Details ON Meter_Reading.Task_Id = Task_Details.Auto_Id WHERE Meter_Reading.Task_Id ='"+TaskID+"' AND Meter_Reading.UpdatedStatus = 'no'  " ;

                    Cursor cursorMeterReading = database.rawQuery(selectQuery1, null);

                    if (cursorMeterReading.moveToFirst()) {
                        do {
                            JSONObject MeterReadingJsonObject = new JSONObject();

                            MeterReadingJsonObject.put("Task_Id",cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Task_Id")));
                            MeterReadingJsonObject.put("Asset_Id", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Asset_Id")));
                            MeterReadingJsonObject.put("Form_Structure_Id", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Form_Structure_Id")));
                            MeterReadingJsonObject.put("Reading", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Reading")));
                            MeterReadingJsonObject.put("UOM", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("UOM")));
                            MeterReadingJsonObject.put("Reset", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Reset")));
                            MeterReadingJsonObject.put("Activity_Frequency_Id", cursorMeterReading.getString(cursorMeterReading.getColumnIndex("Activity_Frequency_Id")));
                            MeterReadingJsonObject.put("Record_Status",loggerData[0] );
                            MeterReadingJsonObject.put("Last_Date_Time",loggerData[1]);
                            MeterReadingJsonObject.put("Last_IP",loggerData[2]);
                            MeterReadingJsonObject.put("Last_User_Id",loggerData[3]);
                            MeterReadingJsonObject.put("Update_Location",loggerData[4]);
                            MeterReadingJsonObject.put("Apk_Web_Version", loggerData[5]);
                            MeterReadingJsonObject.put("GeoLoc", loggerData[6]);
                            MeterReadingArray.put(MeterReadingJsonObject);
                        } while (cursorMeterReading.moveToNext());

                    }
                    cursorMeterReading.close();

                    String selectQueryData = "SELECT  Data_Posting.* FROM Data_Posting LEFT JOIN  Task_Details ON Data_Posting.Task_Id = Task_Details.Auto_Id WHERE Data_Posting.UpdatedStatus = 'no' AND Data_Posting.Task_Id ='"+TaskID+"' ";
                    Cursor cursorDataPosting = database.rawQuery(selectQueryData, null);
                    if (cursorDataPosting.moveToFirst()) {
                        do {
                            JSONObject DatPostingJsonObject = new JSONObject();
                            DatPostingJsonObject.put("Task_Id", cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Task_Id")));
                            DatPostingJsonObject.put("Form_Id",cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Form_Id")));
                            DatPostingJsonObject.put("Form_Structure_Id",cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Form_Structure_Id")));
                            DatPostingJsonObject.put("Parameter_Id", cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Parameter_Id")));
                            DatPostingJsonObject.put("Value", cursorDataPosting.getString(cursorDataPosting.getColumnIndex("Value")));
                            DatPostingJsonObject.put("Record_Status",loggerData[0] );
                            DatPostingJsonObject.put("Last_Date_Time",loggerData[1]);
                            DatPostingJsonObject.put("Last_IP",loggerData[2]);
                            DatPostingJsonObject.put("Last_User_Id",loggerData[3]);
                            DatPostingJsonObject.put("Update_Location",loggerData[4]);
                            DatPostingJsonObject.put("Apk_Web_Version", loggerData[5]);
                            DatPostingJsonObject.put("GeoLoc", loggerData[6]);
                            DataPostingArray.put(DatPostingJsonObject);
                        } while (cursorDataPosting.moveToNext());
                    }
                    cursorDataPosting.close();

                    String selectQueryAlert = "SELECT * FROM AlertMaster WHERE Task_Id='"+TaskID+"'";
                    Cursor cursorAlert = database.rawQuery(selectQueryAlert, null);
                    if (cursorAlert.moveToFirst()) {
                        do {
                            JSONObject AlertJsonObject = new JSONObject();
                            AlertJsonObject.put("Task_Id",cursorAlert.getString(cursorAlert.getColumnIndex("Task_Id")));
                            AlertJsonObject.put("Form_Id", cursorAlert.getString(cursorAlert.getColumnIndex("Form_Id")));
                            AlertJsonObject.put("Form_Structure_Id", cursorAlert.getString(cursorAlert.getColumnIndex("Form_Structure_Id")));
                            AlertJsonObject.put("Alert_Type", cursorAlert.getString(cursorAlert.getColumnIndex("Alert_Type")));
                            AlertJsonObject.put("Created_By_Id", cursorAlert.getString(cursorAlert.getColumnIndex("Created_By_Id")));
                            AlertJsonObject.put("TaskType", cursorAlert.getString(cursorAlert.getColumnIndex("TaskType")));
                            AlertJsonObject.put("Critical", cursorAlert.getString(cursorAlert.getColumnIndex("Critical")));
                            AlertJsonObject.put("Record_Status",loggerData[0] );
                            AlertJsonObject.put("Last_Date_Time",loggerData[1]);
                            AlertJsonObject.put("Last_IP",loggerData[2]);
                            AlertJsonObject.put("Last_User_Id",loggerData[3]);
                            AlertJsonObject.put("Update_Location",loggerData[4]);
                            AlertJsonObject.put("Apk_Web_Version", loggerData[5]);
                            AlertJsonObject.put("GeoLoc", loggerData[6]);
                            AlertArray.put(AlertJsonObject);
                        } while (cursorAlert.moveToNext());
                    }
                    cursorAlert.close();
                    TaskJsonArray.put(TaskJsonObject);
                } while (cursor.moveToNext());
            }cursor.close();
            database.close();

        }catch (Exception E){
            E.printStackTrace();
        }

        try {

            TaskDetails.put("TaskDetails",TaskJsonArray);
            MeterReading.put("MeterReading",MeterReadingArray);
            DataValue.put("DataValue",DataPostingArray);
            AlertData.put("AlertData",AlertArray);

            UploadArray.put(TaskDetails);
            UploadArray.put(MeterReading);
            UploadArray.put(DataValue);
            UploadArray.put(AlertData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return UploadArray;
    }


    public JSONArray composeJSONforTicket(String UserId){
        JSONArray TicketJsonArray = new JSONArray();

        String[]loggerData = new LoggerFile().loggerFunction(UserId);
        String selectQuery = "SELECT  * FROM Ticket_Master  WHERE UpdatedStatus = 'no' AND Ticket_Raise_By='"+UserId+"'";

        try {
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    JSONObject TicketJsonObject = new JSONObject();
                    TicketJsonObject.put("ID", cursor.getString(cursor.getColumnIndex("ID")));
                    TicketJsonObject.put("Site_Location_Id",  cursor.getString(cursor.getColumnIndex("Site_Location_Id")));
                    TicketJsonObject.put("Company_Customer_Id",  cursor.getString(cursor.getColumnIndex("Company_Customer_Id")));
                    TicketJsonObject.put("Created_Source",  cursor.getString(cursor.getColumnIndex("Created_Source")));
                    TicketJsonObject.put("Created_At",  cursor.getString(cursor.getColumnIndex("Created_At")));
                    TicketJsonObject.put("ticket_Subject",  cursor.getString(cursor.getColumnIndex("ticket_Subject")));
                    TicketJsonObject.put("ticket_Content",  cursor.getString(cursor.getColumnIndex("ticket_Content")));
                    TicketJsonObject.put("ticket_Priority",  cursor.getString(cursor.getColumnIndex("ticket_Priority")));
                    TicketJsonObject.put("Ticket_Raise_By", cursor.getString(cursor.getColumnIndex("Ticket_Raise_By")));
                    TicketJsonArray.put(TicketJsonObject);
                } while (cursor.moveToNext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return TicketJsonArray;
    }



    public JSONArray composeJSONforAssets(String UserId){
        JSONArray AssetJsonArray = new JSONArray();
        String selectQuery = "SELECT  * FROM Asset_Details where UpdatedStatus = 'no'";
        try {
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    String[]loggerData = new LoggerFile().loggerFunction(UserId);
                    JSONObject AssetJsonObject = new JSONObject();
                    AssetJsonObject.put("Asset_Id", cursor.getString(cursor.getColumnIndex("Asset_Id")));
                    AssetJsonObject.put("Asset_Status_Id", cursor.getString(cursor.getColumnIndex("Asset_Status_Id")));
                    AssetJsonObject.put("Manual_Time", cursor.getString(cursor.getColumnIndex("Manual_Time")));
                    AssetJsonObject.put("Asset_Update_Time", cursor.getString(cursor.getColumnIndex("Asset_Update_Time")));
                    AssetJsonArray.put(AssetJsonObject);
                } while (cursor.moveToNext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return AssetJsonArray;
    }

    public JSONArray composeJSONforAssetsStatusChange(String SiteId,String UserId){
        JSONArray AssetJsonArray = new JSONArray();
        String selectQuery = "SELECT  * FROM AssetStatusLog where Site_Location_Id='"+SiteId+"' AND Assigned_To_User_Id='"+UserId+"' AND UpdatedStatus = 'no'";
        try {
            SQLiteDatabase database = this.getWritableDatabase();
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                do {
                    String[]loggerData = new LoggerFile().loggerFunction(UserId);
                    JSONObject AssetJsonObject1 = new JSONObject();
                    AssetJsonObject1.put("ID", cursor.getString(cursor.getColumnIndex("Id")));
                    AssetJsonObject1.put("Asset_Id", cursor.getString(cursor.getColumnIndex("Asset_Id")));
                    AssetJsonObject1.put("Site_Location_Id", cursor.getString(cursor.getColumnIndex("Site_Location_Id")));
                    AssetJsonObject1.put("Previous_Asset_Status_Id", cursor.getString(cursor.getColumnIndex("Previous_Asset_Status_Id")));
                    AssetJsonObject1.put("Asset_Status_Id", cursor.getString(cursor.getColumnIndex("Asset_Status_Id")));
                    AssetJsonObject1.put("Manual_Time", cursor.getString(cursor.getColumnIndex("Manual_Time")));
                    AssetJsonObject1.put("Asset_Updated_Time", cursor.getString(cursor.getColumnIndex("Asset_Updated_Time")));
                    AssetJsonObject1.put("Remark", cursor.getString(cursor.getColumnIndex("Remark")));
                    AssetJsonObject1.put("Assigned_To_User_Id",cursor.getString(cursor.getColumnIndex("Assigned_To_User_Id")));
                    AssetJsonArray.put(AssetJsonObject1);
                } while (cursor.moveToNext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return AssetJsonArray;
    }

    public JSONArray SyncInfo(String UserId,int Battery_Percentage,int Signal_Strength,Character Network_Type,String CarrierName,String DeviceDetails){

        JSONArray TicketJsonArray = new JSONArray();
        String[]loggerData = new LoggerFile().loggerFunction(UserId);
        try {
            JSONObject TicketJsonObject = new JSONObject();
            TicketJsonObject.put("Sync_Date_Time",loggerData[1]);
            TicketJsonObject.put("MacAddress",loggerData[2]);
            TicketJsonObject.put("User_Id",loggerData[3]);
            TicketJsonObject.put("Apk_Web_Version", loggerData[5]);
            TicketJsonObject.put("GeoLoc", loggerData[6]);
            TicketJsonObject.put("Signal_Strength",Signal_Strength);
            TicketJsonObject.put("Battery_Percentage",Battery_Percentage);
            TicketJsonObject.put("Network_Type",Network_Type);
            TicketJsonObject.put("Carrier_Name",CarrierName);
            TicketJsonObject.put("Device_Name",DeviceDetails);
            TicketJsonArray.put(TicketJsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return TicketJsonArray;
    }

    public boolean insertBitmap(Bitmap selfie, String taskID,String Type,String SiteId)  {
        boolean image = false;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        selfie.compress(Bitmap.CompressFormat.JPEG, 100, out);

        ByteArrayOutputStream meterOut = new ByteArrayOutputStream();
        byte[] selfieBuffer=out.toByteArray();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values;
        try
        {
            values = new ContentValues();
            values.put("Image_Selfie", selfieBuffer);
            values.put("Task_Id", taskID);
            values.put("Image_Type", Type);
            values.put("UpdatedStatus","no");
            values.put("Site_Location_Id",SiteId);
            long i = db.insert("Task_Selfie", null, values);
            if(i == -1)
                image =  false;
            else
                image =  true;
            Log.i("Insert", i + "");
            // Insert into database successfully.
        }
        catch (SQLiteException e)
        {
            e.printStackTrace();

        }
        return image;


    }

    public HashMap<String,String> Images(String TaskId){
        byte[] image = null;
        String image_str,type;
        HashMap<String,String> images = new HashMap<>();
        String selectQuery = "SELECT  * FROM Task_Selfie where Task_Id='" +TaskId + "'";

        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                image = cursor.getBlob(cursor.getColumnIndex("Image_Selfie"));
                image_str = Base64.encodeToString(image, Base64.DEFAULT);
                type = cursor.getString(cursor.getColumnIndex("Image_Type"));
                images.put(image_str,type);
            } while (cursor.moveToNext());
        }
        return images;
    }

    public void updateImages(String TaskId, String status){
        try {
            SQLiteDatabase database = this.getWritableDatabase();
            String updateQuery = "Update Task_Selfie set UpdatedStatus = '"+ status +"' where Task_Id ='"+ TaskId +"'";
            database.execSQL(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
